# cc98api
CC98 V2 API

## 你可以用它来干啥

只要浏览器能看到/做到的 都能使用API得到

* 获取十大帖子列表、获取新帖列表、获取广告列表
* 获取帖子内容
* 编辑一个回复
* 签到

## 立即使用

```
username = "..."
password = "..."
from cc98api_base import CC98_API_V2
x=CC98_API_V2(username,password)
x.login()
print(x.signin_status())
```

## 文档

```
    __init__(username, password, proxy=None): 构造函数需要传入用户名和密码 还可以传入一个proxy的数组 在403时随机选择代理进行访问 现在构造函数不会自动登录
    fetch(url): 给定url 使用登录状态GET访问url 并返回json
        fetch_real(url): 当不想被缓存的时候用这个 会使用登录状态
        fetch_real_real(url): 无需登录的请求用这个 不会触发登录
    login(): 发起登录请求 更新请求使用的access_token
    topic_new(from_=0, size=20): 查看新帖 返回帖子的数组
    board_topic(boardid, from_=0, size=20): 获取版面帖子列表 返回帖子的数组
    topic_hot(): 获取十大列表 返回十大帖子的数组
    topic(id): 单个帖子 返回帖子
    board(boardid): 单个板块 返回板块
    topic_post(id): 帖子的内容/回复 返回回复的数组
    post_edit(id, content, title, contenttype=0, type=0): 编辑一条回复
    upload(fp): 上传一张图片
    signin(data="sign in!"): 签到
    signin_status(): 获取当前签到状态
    get_wealth(): 获取当前财富值
    userinfos(userids): 获取用户信息 输入userid的集合/数组，返回{userid: 用户信息}
    get_user_topics(self, userid, _from=0, size=20): 获取用户最近的发帖
    userinfobyname(username): 根据用户名获取用户信息
    topic_reply(id, content, title="", contenttype=0): 对帖子进行回复
    index(): 返回首页显示的信息
    ads(): 返回广告的数组
    board_all(): 返回板块分类的数组 包含所有板块的基本信息
        board_all2(): 返回所有版块的dict，不包含分类信息
    post_reward(id, reason="好文章", wealth=1000): 对回复发米
    my_favorite(page=1): 返回收藏，第page页，每页20条
    add_favorite(id): 添加收藏，返回True或False
    remove_favorite(id): 移除收藏，返回True或False
    unread_count(): 查询未读通知数量，返回{"replyCount":0, "atCount":0, "systemCount":0, "messageCount":0}
    new_topic(board, title, content, contentType=0, type=0): 在board板块发新帖 返回新帖子的id int
```

### 代理说明

在构造函数时可以指定proxy参数，该参数为代理的列表，代理需要支持https访问目标域名。在API请求接连遇到两次403后（可能98封杀了IP）即随机选择其中一个代理进行使用。

如`["http://10.0.0.0:8118", "socks5://127.0.0.1:1080"]` 


### 缓存功能

在需要保证API调用的耗时较低时(如实现不卡的旧版98页面)，可以使用子类Cache_RW_CC98_API_V2

在爬取98时可以顺带写缓存 使用只写缓存(不读取缓存)的子类Cache_WO_CC98_API_V2

缓存功能依赖于`redis`服务器和`redis`、`msgpack-python`包，如果你不需要缓存功能(只使用`CC98_API_V2`类) 你可以删掉最后的子类class和附近的import

## 依赖

* requests
* msgpack
* redis

## 类似项目

[Android ApiCC98/CC98APIInterface.java](https://github.com/6769/ApiCC98/blob/master/src/main/java/win/pipi/api/network/CC98APIInterface.java)

[Android mycc98/AJAXFetch.java](https://github.com/Dearkano/MyCC98/blob/master/app/src/main/java/com/example/tianzijun/mycc98/AJAXFetch.java)