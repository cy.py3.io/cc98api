# encoding: utf-8
from cc98api_base import CC98_API_V2, CC98_API_V2_OAUTH, LoginError

# 以下代码均为缓存相关的子类，请移步cc98api_base.py查看API调用逻辑
# 如果你不需要缓存，在使用时可以from cc98api_base import CC98_API_V2
# 需要缓存则 from cc98api import Cache_WO_CC98_API_V2, Cache_RW_CC98_API_V2

from redis_cache import cache_it_json, SimpleCache
from config import REDIS_HOST, REDIS_PORT, REDIS_PASSWORD
my_cache = SimpleCache(limit=10000, expire=60 * 60 * 24 * 7, hashkeys=True, host=REDIS_HOST, port=REDIS_PORT, password=REDIS_PASSWORD, db=1, namespace='CC98API')
import random

class Cache_WO_CC98_API_V2(CC98_API_V2):
    """
    Write only to cache
    """
    @cache_it_json(cache=my_cache, writeonly=True)
    def fetch(self, url):
        return super().fetch(url)

    # 查看新帖 返回帖子的数组
    def topic_new(self, from_=0, size=20):
        result = self.fetch("topic/new?from={from_}&size={size}".format(**locals()))
        if not hasattr(self, "topicdata"):
            self.topicdata = {}
        for item in result:
            self.topicdata[str(item["id"])] = item
            self.topic(str(item["id"])) #在获取新帖的时候就能得到帖子信息，顺带对topic加以缓存
        return result
    
    # 获取版面帖子列表
    def board_topic(self, boardid, from_=0, size=20):
        result = self.fetch("Board/{boardid}/topic?from={from_}&size={size}".format(**locals()))
        if not hasattr(self, "topicdata"):
            self.topicdata = {}
        for item in result:
            self.topicdata[str(item["id"])] = item
            self.topic(str(item["id"])) # 写入redis缓存
        return result

    # 单个帖子 返回帖子 id为str类型
    @cache_it_json(cache=my_cache, writeonly=True)
    def topic(self, id):
        if not hasattr(self, "topicdata"):
            self.topicdata = {}
        if id in self.topicdata:
            return self.topicdata[id]
        else:
            return self.fetch_real("Topic/"+id)

class Cache_RW_CC98_API_V2(CC98_API_V2):
    """
    read from and write to cache
    """
    @cache_it_json(cache=my_cache)
    def fetch(self, url):
        return super().fetch(url)

    # 此id需要为str类型
    @cache_it_json(cache=my_cache)
    def topic(self, id):
        return self.fetch_real("Topic/"+id)
        
    # 获取一个用户信息，但为了隐藏我们要获取哪个用户 随机选择一些用户同时请求 返回{userid: 用户信息}
    def userinfo_oblivious_fetch(self, userid):
        userid = int(userid)
        args = set([userid])
        args.update(random.sample(range(1,600000),10))
        return self.userinfos(args)
    
    # userid: int
    @cache_it_json(cache=my_cache)
    def userinfo_one(self, userid):
        if not hasattr(self, "userdata"):
            self.userdata = {}
        if userid in self.userdata:
            return self.userdata[userid]
        data = self.userinfo_oblivious_fetch(userid)
        self.userdata.update(data)
        for key in data:
            self.userinfo_one(key)#write cache
        return data[userid]

Cache_WO_CC98_API_V2_OAUTH = type("Cache_WO_CC98_API_V2_OAUTH", (CC98_API_V2_OAUTH,), dict(Cache_WO_CC98_API_V2.__dict__))
Cache_RW_CC98_API_V2_OAUTH = type("Cache_RW_CC98_API_V2_OAUTH", (CC98_API_V2_OAUTH,), dict(Cache_RW_CC98_API_V2.__dict__))
